<?php
include("db.php");

$result = mysqli_query($con, "select user_id,first_name,last_name,email,password,address1,address2,mobile from user_info ") or die("query 1 incorrect.......");

list($user_id, $first_name, $last_name, $email, $user_password, $address1, $address2, $mobile) = mysqli_fetch_array($result);

if (isset($_POST['btn_save'])) {

	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$email = $_POST['email'];
	$user_password = $_POST['password'];
	$address1 = $_POST['address1'];
	$address2 = $_POST['address2'];
	$mobile = $_POST['mobile'];

	mysqli_query($con, "update user_info set first_name='$first_name', last_name='$last_name', email='$email', password='$user_password' , address1='$address1', address2='$address2',mobile='$mobile'  where user_id='$user_id'") or die("Query 2 is inncorrect..........");

	mysqli_close($con);
}
include "header.php";
?>
<!-- End Navbar -->
<div class="main main-raised">
	<!-- SECTION -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">

				<!-- section title -->
				<div class="card-header card-header-primary">
					<h4 class="card-title">จัดการสมาชิก</h4>
				</div>
				<!-- /section title -->

				<!-- Products tab & slick -->
				<div class="card ">
					<hr style="width:20%" align="left">
					<form action="profile.php" name="form" method="post" enctype="multipart/form-data">
						<div class="card-body">
							<div class="table-responsive ps">
								<table class="table tablesorter table-hover" id="">
									<tbody>
										<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" />
										<div class="col-md-12 ">
											<div class="form-group">
												<label>ชื่อ</label> <input type="text" id="first_name" name="first_name" class="form-control" value="<?php echo $first_name; ?>">
											</div>
										</div>
										<div class="col-md-12 ">
											<div class="form-group">
												<label>นามสกุล</label> <input type="text" id="last_name" name="last_name" class="form-control" value="<?php echo $last_name; ?>">
											</div>
										</div>
										<div class="col-md-12 ">
											<div class="form-group">
												<label for="exampleInputEmail1">อีเมลล์</label> <input type="email" id="email" name="email" class="form-control" value="<?php echo $email; ?>">
											</div>
										</div>
										<div class="col-md-12 ">
											<div class="form-group">
												<label>รหัสผ่าน</label> <input type="password" name="password" id="password" class="form-control" value="<?php echo $user_password; ?>">
											</div>
										</div>

										<div class="col-md-12 ">
											<div class="form-group">
												<label for="exampleInputEmail1">เลขที่ไปรษณีย์</label> <input type="text" id="address1" name="address1" class="form-control" value="<?php echo $address1; ?>">
											</div>
										</div>

										<div class="col-md-12 ">
											<div class="form-group">
												<label for="exampleInputEmail1">ที่อยู่</label> <textarea type="text" id="address2" name="address2" class="form-control"><?php echo $address2; ?></textarea>
											</div>
										</div>

										<div class="col-md-12 ">
											<div class="form-group">
												<label for="exampleInputEmail1">เบอร์โทร</label> <input type="text" id="mobile" name="mobile" class="form-control" value="<?php echo $mobile; ?>">
											</div>
										</div>



										<div class="card-footer" align="center">
											<button type="submit" id="btn_save" name="btn_save" class="btn btn-fill btn-primary">บันทึก</button>
										</div>

									</tbody>
								</table>
								<div class="ps__rail-x" style="left: 0px; bottom: 0px;">
									<div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
								</div>
								<div class="ps__rail-y" style="top: 0px; right: 0px;">
									<div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- Products tab & slick -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>


	<!-- SECTION -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">

				<!-- section title -->
				<div class="card-header card-header-primary">
					<h4 class="card-title">ยืนยันคำสั่งซื้อ</h4>
				</div>
				<!-- /section title -->

				<!-- Products tab & slick -->
				<div class="card ">
					<div class="card-body">
						<div class="table-responsive ps">
							<table class="table table-hover tablesorter " id="">
								<thead class=" text-primary">
									<tr>
										<th>สินค้า</th>
										<th>จำนวน</th>
										<th>ราคา</th>
										<th>สถานะ</th>
										<th>ยืนยันคำสั่งซื้อ</th>
										<th>ดูรายละเอียดคำสั่งซื้อ</th>
										<th>ลบคำสั่งซื้อ</th>
									</tr>
								</thead>
								<tbody>

									<tr>
										<td>สารปรับปรุงขี้แดดนาเกลือ</td>
										<td>1</td>
										<td>500 บาท</td>
										<td>ยังไม่ยืนยัน</td>

										<td>
											<a class=' btn btn-success'>ยืนยันคำสั่งซื้อ</a>
										</td>
										<td>
											<a class=' btn btn-primary'>รายละเอียดคำสั่งซื้อ</a>
										</td>
										<td>
											<a class=' btn btn-danger'>ลบ</a>
										</td>
									</tr>


								</tbody>
							</table>

							<div class="ps__rail-x" style="left: 0px; bottom: 0px;">
								<div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
							</div>
							<div class="ps__rail-y" style="top: 0px; right: 0px;">
								<div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
							</div>
						</div>
					</div>
				</div>
				<!-- Products tab & slick -->

				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->
	</div>
</div>


<br>
<?php
include "footer.php";
?>